﻿using Harmony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;
using static CustomMissText.Utils.ReflectionUtil;

namespace CustomMissText.HarmonyPatches
{
    [HarmonyPatch(typeof(MissedNoteEffectSpawner), "HandleNoteWasMissed",
        new Type[] { 
            typeof(BeatmapObjectSpawnController),
            typeof(NoteController)})]
    class MissedNoteEffectSpawnerHandleNoteWasMissed
    {
        public static bool inMethod = false;
        public static FlyingTextSpawner _spawner;
        public static FlyingTextSpawner spawner
        {
            get
            {
                if (_spawner != null) return _spawner;

                // create FlyingTextSpawner; will be missing the prefab it needs, so it's time for a heist!
                _spawner = new GameObject("CustomMissTextSpawner").AddComponent<FlyingTextSpawner>();

                // go steal all the Zenject installers
                var installers = UnityEngine.Object.FindObjectsOfType<MonoInstallerBase>();
                foreach (var installer in installers)
                {
                    // rifle through the installers' pockets for precious DiContainers, which may hold the secret of FlyingTextSpawning
                    var container = installer.getPrivateProperty<DiContainer>("Container");

                    if (container != null // some installers are poor and have no containers, this is very sad
                        && container.HasBinding<FlyingTextEffect.Pool>()) // some containers have only worthless secrets
                    {
                        container.Inject(_spawner); // gives our FlyingTextSpawner all it needs to function
                        break; // stop looting, we have what we need
                    }
                }
                _spawner.setPrivateField("_color", new Color(1, 0, 0));
                _spawner.setPrivateField("_fontSize", 3f);

                return _spawner;
            }
        }
        static bool Prefix(MissedNoteEffectSpawner __instance, NoteController noteController, float ____spawnPosZ)
        {
            if (spawner == null)
            {
                Console.WriteLine("Failed to inject FlyingTextSpawner!");
                return true;
            }

            // remainder of the method copied from decompiled code from a previous version of Beat Saber, from before the miss text became an image
            NoteData noteData = noteController.noteData;
            if (noteData.noteType == NoteType.NoteA || noteData.noteType == NoteType.NoteB)
            {
                Vector3 vector = noteController.noteTransform.position;
                Quaternion worldRotation = noteController.worldRotation;
                vector = noteController.inverseWorldRotation * vector;
                vector.z = ____spawnPosZ;
                vector = worldRotation * vector;
                inMethod = true;
                spawner.SpawnText(vector, noteController.worldRotation, noteController.inverseWorldRotation, "MISS");
                inMethod = false;
            }
            return false;
        }
    }
}
