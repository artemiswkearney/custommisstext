﻿using Harmony;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace CustomMissText.HarmonyPatches
{
    [HarmonyPatch(typeof(FlyingTextEffect), "InitAndPresent",
        new Type[] { 
            typeof(string),
            typeof(float),
            typeof(Vector3),
            typeof(Quaternion),
            typeof(Color),
            typeof(float),
            typeof(bool)})]
    class FlyingTextEffectInitAndPresent
    {
        static bool Prefix(ref string text, ref TextMeshPro ____text)
        {
            if (MissedNoteEffectSpawnerHandleNoteWasMissed.inMethod && text == "MISS")
            {
                // Choose an entry randomly

                // Unity's random seems to give biased results
                // int entryPicked = UnityEngine.Random.Range(0, entriesInFile.Count);
                // using System.Random instead
                int entryPicked = Plugin.random.Next(Plugin.allEntries.Count);

                // Set the text
                text = String.Join("\n", Plugin.allEntries[entryPicked]);

                // Prevent undesired word wrap
                ____text.overflowMode = TextOverflowModes.Overflow;
                ____text.enableWordWrapping = false;

                // Enable formatting
                ____text.richText = true;
            }
            return true;
        }
    }
}
