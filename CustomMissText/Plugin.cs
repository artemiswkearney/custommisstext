﻿using Harmony;
using IllusionPlugin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CustomMissText
{
    public class Plugin : IPlugin
    {
        // path to the file to load text from
        private const string FILE_PATH = "/UserData/CustomMissText.txt";

        // used if we can't load any custom entries
        public static readonly string[] DEFAULT_TEXT = { "MISS" };

        // used if no config file exists
        public const string DEFAULT_CONFIG =
@"# Custom Miss Text v1.0.3
# by Arti
#
# Use # for comments!
# Separate entries with empty lines; a random one will be picked each time the menu loads.
HECK

F

OwO

HITN'T

OOF

MS.

115
<size=50%>just kidding</size>

HISS

NOPE

WHOOSH

OOPS

C-C-C-COMBO
BREAKER

I MEANT TO
DO THAT

MOSS

MASS

MESS

MUSS

MYTH

KISS

# The following lines suggested by @E8 on BSMG
MISSCLICK

HIT OR MISS

LAG

TRACKING

LUL";

        // caches entries loaded from the file so we don't need to do IO every time the text is displayed
        public static List<string[]> allEntries = null;

        // caches one instance of System.Random so we aren't instantiating them constantly
        public static System.Random random;

        public string Name => "CustomMissText";
        public string Version => "1.0.3";
        public void OnApplicationStart()
        {
            try
            {
                var harmony = HarmonyInstance.Create("com.arti.BeatSaber.CustomMissText");
                harmony.PatchAll(Assembly.GetExecutingAssembly());
            }
            catch (Exception e)
            {
                Console.WriteLine("[CustomMissText] This plugin requires Harmony. Make sure the Harmony " +
                    "library is installed.");
                Console.WriteLine(e);
            }
            random = new System.Random();
            reloadFile();
        }
        
        public void OnApplicationQuit()
        {
        }

        public void OnLevelWasLoaded(int level)
        {
        }

        public void OnLevelWasInitialized(int level)
        {
        }

        public void OnUpdate()
        {
        }

        public void OnFixedUpdate()
        {
        }

        public static List<string[]> readFromFile(string relPath)
        {
            List<string[]> entriesInFile = new List<string[]>();

            // Look for the custom text file
            string gameDirectory = Environment.CurrentDirectory;
            gameDirectory = gameDirectory.Replace('\\', '/');
            if (File.Exists(gameDirectory + relPath))
            {
                var linesInFile = File.ReadLines(gameDirectory + relPath, new UTF8Encoding(true));

                // Strip comments (all lines beginning with #)
                linesInFile = linesInFile.Where(s => s == "" || s[0] != '#');

                // Collect entries, splitting on empty lines
                List<string> currentEntry = new List<string>();
                foreach (string line in linesInFile)
                {
                    if (line == "")
                    {
                        entriesInFile.Add(currentEntry.ToArray());
                        currentEntry.Clear();
                    }
                    else
                    {
                        currentEntry.Add(line);
                    }
                }
                if (currentEntry.Count != 0)
                {
                    // in case the last entry doesn't end in a newline
                    entriesInFile.Add(currentEntry.ToArray());
                }
                if (entriesInFile.Count == 0)
                {
                    // No entries; warn and continue
                    Console.WriteLine("[CustomMissText] File found, but it contained no entries!");
                }
            }
            else
            {
                // No custom text file found!
                // Create the file and populate it with the default config
                try
                {
                    using (FileStream fs = File.Create(gameDirectory + relPath))
                    {
                        Byte[] info = new UTF8Encoding(true).GetBytes(DEFAULT_CONFIG
                            // normalize newlines to CRLF
                            .Replace("\r\n", "\n").Replace("\r", "\n").Replace("\n", "\r\n"));
                        fs.Write(info, 0, info.Length);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("[CustomMissText] No custom text file found, and an error was encountered trying to generate a default one!");
                    Console.WriteLine("[CustomMissText] Error:");
                    Console.WriteLine(ex);
                    Console.WriteLine("[CustomMissText] To use this plugin, manually create the file " + relPath + " in your Beat Saber install directory.");
                    return entriesInFile;
                }
                // File was successfully created; load from it with a recursive call.
                return readFromFile(relPath);
            }

            return entriesInFile;
        }

        public void reloadFile()
        {
            allEntries = readFromFile(FILE_PATH);
        }
    }
}
