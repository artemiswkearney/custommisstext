Plugin that changes the text when you miss a note in Beat Saber.  
Picks a random entry from a text file every time you miss. Run the game once with the plugin installed to generate an example file, demonstrating the format; see [the TextMeshPro docs](http://digitalnativestudios.com/textmeshpro/docs/rich-text/) for more ways to customize your custom text. Edit the file at `Beat Saber/UserData/CustomMissText.txt`.  


[How to install Beat Saber mods](https://bsmg.wiki/beginners-guide) | [Support the creator](https://ko-fi.com/artibs) and [other modders](https://bsmg.wiki/about)